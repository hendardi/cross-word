﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Cross_Word
{
    class Program
    {
        static List<map> arrMap = new List<map>();
        static bool sukses = false;

        static void Main(string[] args)
        {
            map m = null;
            kata k = null;

            string inp;
            int menu, map, pilihMap = 0, deleteMap, kotak, buatKotak, deleteKotak, kata, taruhKata, indexDelete, delKata;
            int tempPanjang = 0, tempLebar = 0;
            int xkotak, ykotak;
            int xbawal, xbakhir, ybawal, ybakhir;
            int xdawal, xdakhir, ydawal, ydakhir;
            int x, y;
            int arah;

            Console.WriteLine("PROJECT : ");
            Console.WriteLine("         STRUKTUR DATA DAN ALGORTIMA PEMOGRAMAN 2");
            Console.WriteLine("                 ===TEKA - TEKI SILANG===");
            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();

            do
            {
                Console.Clear();
                if (arrMap.Count > 0)
                {
                    arrMap[pilihMap].cetakMap();
                    Console.WriteLine();
                }

                Console.WriteLine("1. Map");
                Console.WriteLine("2. Kotak");
                Console.WriteLine("3. Kata");
                Console.WriteLine("4. Solve");
                Console.WriteLine("5. Save");
                Console.WriteLine("6. Load");
                Console.WriteLine("7. Exit");
                Console.Write("Pilihan Anda : ");
                menu = int.Parse(Console.ReadLine());

                if (menu == 0)
                {
                    sorting(arrMap[pilihMap]);
                    foreach (string item in arrMap[pilihMap].daftarKata)
                    {
                        Console.WriteLine(item);
                    }
                    Console.ReadLine();
                }
                else if (menu == 1)
                {
                    Console.WriteLine("1. Buat Map");
                    Console.WriteLine("2. Pilih Map");
                    Console.WriteLine("3. Delete Map");
                    Console.Write("Pilihan Anda : ");
                    map = int.Parse(Console.ReadLine());

                    if (map == 1)
                    {
                        Console.Write("Masukkan Panjang : ");
                        tempPanjang = int.Parse(Console.ReadLine());
                        Console.Write("Masukkan Lebar   : ");
                        tempLebar = int.Parse(Console.ReadLine());

                        m = new map(tempPanjang, tempLebar);
                        arrMap.Add(m);
                    }
                    else if (map == 2)
                    {
                        if (arrMap.Count > 0)
                        {
                            for (int i = 0; i < arrMap.Count; i++)
                            {
                                Console.WriteLine((i + 1) + ". Map ke - " + (i + 1));
                                Console.WriteLine("   Panjang : " + arrMap[i].panjang);
                                Console.WriteLine("   Lebar   : " + arrMap[i].lebar);
                                Console.WriteLine("==================");
                            }
                            Console.Write("Pilih Map : ");
                            pilihMap = int.Parse(Console.ReadLine());
                            pilihMap--;
                        }
                        else
                        {
                            Console.WriteLine("Belum ada map!");
                            Console.ReadLine();
                        }
                    }
                    else if (map == 3)
                    {
                        if (arrMap.Count > 0)
                        {
                            for (int i = 0; i < arrMap.Count; i++)
                            {
                                Console.WriteLine((i + 1) + ". Map ke - " + (i + 1));
                                Console.WriteLine("   Panjang : " + arrMap[i].panjang);
                                Console.WriteLine("   Lebar   : " + arrMap[i].lebar);
                                Console.WriteLine("==================");
                            }
                            Console.Write("Delete : ");
                            deleteMap = int.Parse(Console.ReadLine());
                            deleteMap--;
                            arrMap[deleteMap].deleteMap(deleteMap, arrMap);

                            if (deleteMap <= pilihMap)
                            {
                                pilihMap--;
                                if (pilihMap <= 0)
                                {
                                    pilihMap = 0;
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("Belum ada map!");
                            Console.ReadLine();
                        }
                    }
                }
                else if (menu == 2)
                {
                    if (arrMap.Count > 0)
                    {
                        Console.WriteLine("1. Buat Kotak");
                        Console.WriteLine("2. Delete Kotak");
                        Console.Write("Pilihan Anda : ");
                        kotak = int.Parse(Console.ReadLine());

                        if (kotak == 1)
                        {
                            Console.WriteLine("1. Horizontal");
                            Console.WriteLine("2. Vertikal");
                            Console.Write("Pilihan anda : ");
                            buatKotak = int.Parse(Console.ReadLine());

                            if (buatKotak == 1)
                            {
                                Console.Write("X awal  : ");
                                xbawal = int.Parse(Console.ReadLine());
                                Console.Write("X akhir : ");
                                xbakhir = int.Parse(Console.ReadLine());
                                Console.Write("Y       : ");
                                ykotak = int.Parse(Console.ReadLine());

                                if ((xbawal >= 0 && xbakhir < arrMap[pilihMap].panjang) && (ykotak >= 0 && ykotak < arrMap[pilihMap].lebar))
                                {
                                    for (int i = xbawal; i <= xbakhir; i++)
                                    {
                                        arrMap[pilihMap].kotak[ykotak, i] = true;
                                        arrMap[pilihMap].kotakKosong[ykotak, i] = true;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Ukuran tidak sesuai dengan map!");
                                    Console.ReadLine();
                                }

                            }
                            else if (buatKotak == 2)
                            {
                                Console.Write("X       : ");
                                xkotak = int.Parse(Console.ReadLine());
                                Console.Write("Y awal  : ");
                                ybawal = int.Parse(Console.ReadLine());
                                Console.Write("Y akhir : ");
                                ybakhir = int.Parse(Console.ReadLine());

                                if ((ybawal >= 0 && ybakhir < arrMap[pilihMap].lebar) && (xkotak >= 0 && xkotak < arrMap[pilihMap].panjang))
                                {
                                    for (int i = ybawal; i <= ybakhir; i++)
                                    {
                                        arrMap[pilihMap].kotak[i, xkotak] = true;
                                        arrMap[pilihMap].kotakKosong[i, xkotak] = true;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Ukuran tidak sesuai dengan map!");
                                    Console.ReadLine();
                                }
                            }
                        }
                        else if (kotak == 2)
                        {
                            Console.WriteLine("1. Horizontal");
                            Console.WriteLine("2. Vertikal");
                            Console.Write("Pilihan Anda : ");
                            deleteKotak = int.Parse(Console.ReadLine());

                            if (deleteKotak == 1)
                            {
                                Console.Write("X awal  : ");
                                xdawal = int.Parse(Console.ReadLine());
                                Console.Write("X akhir : ");
                                xdakhir = int.Parse(Console.ReadLine());
                                Console.Write("Y       : ");
                                ykotak = int.Parse(Console.ReadLine());

                                if ((xdawal >= 0 && xdakhir < arrMap[pilihMap].panjang) && (ykotak >= 0 && ykotak < arrMap[pilihMap].lebar))
                                {
                                    for (int i = xdawal; i <= xdakhir; i++)
                                    {
                                        arrMap[pilihMap].kotak[ykotak, i] = false;
                                        arrMap[pilihMap].kotakKosong[ykotak, i] = false;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Ukuran tidak sesuai dengan map!");
                                    Console.ReadLine();
                                }
                            }
                            else if (deleteKotak == 2)
                            {
                                Console.Write("X       : ");
                                xkotak = int.Parse(Console.ReadLine());
                                Console.Write("Y awal  : ");
                                ydawal = int.Parse(Console.ReadLine());
                                Console.Write("Y akhir : ");
                                ydakhir = int.Parse(Console.ReadLine());

                                if ((ydawal >= 0 && ydakhir < arrMap[pilihMap].lebar) && (xkotak >= 0 && xkotak < arrMap[pilihMap].panjang))
                                {
                                    for (int i = ydawal; i <= ydakhir; i++)
                                    {
                                        arrMap[pilihMap].kotak[i, xkotak] = false;
                                        arrMap[pilihMap].kotakKosong[i, xkotak] = false;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Ukuran tidak sesuai dengan map!");
                                    Console.ReadLine();
                                }
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Belum ada map!");
                        Console.ReadLine();
                    }
                }

                else if (menu == 3)
                {
                    if (arrMap.Count > 0)
                    {
                        Console.WriteLine("1. Tambah Kata");
                        Console.WriteLine("2. Taruh Kata");
                        Console.WriteLine("3. Lihat Kata di Tangan");
                        Console.WriteLine("4. Delete Kata di Tangan");
                        Console.WriteLine("5. Delete Kata di Map");
                        Console.Write("Pilihan Anda : ");
                        kata = int.Parse(Console.ReadLine());

                        if (kata == 1)
                        {
                            Console.Write("Masukkan kata : ");
                            inp = Console.ReadLine();
                            arrMap[pilihMap].daftarKata.Add(inp);
                        }
                        else if (kata == 2)
                        {
                            if (arrMap.Count > 0 && arrMap[pilihMap].daftarKata.Count > 0)
                            {
                                for (int i = 0; i < arrMap[pilihMap].daftarKata.Count; i++)
                                {
                                    Console.WriteLine((i + 1) + ". " + arrMap[pilihMap].daftarKata[i]);
                                }
                                Console.Write("Taruh Kata: ");
                                taruhKata = int.Parse(Console.ReadLine());
                                taruhKata--;
                                Console.Write("Masukkan Koordinat X : ");
                                x = Convert.ToInt32(Console.ReadLine());
                                Console.Write("Masukkan Koordinat Y : ");
                                y = Convert.ToInt32(Console.ReadLine());
                                Console.WriteLine("1. Horizontal");
                                Console.WriteLine("2. Vertikal");
                                Console.Write("Masukkan Arah : ");
                                arah = Convert.ToInt32(Console.ReadLine());
                                k = new kata(arrMap[pilihMap].daftarKata[taruhKata], x, y, arah, arrMap[pilihMap].listKata, arrMap, pilihMap);

                                if (k.validKata == true && k.validPosisi == true)
                                {
                                    arrMap[pilihMap].listKata.Add(k);
                                    arrMap[pilihMap].bListKata.Add(k);
                                    arrMap[pilihMap].daftarKata.RemoveAt(taruhKata);
                                }
                                else
                                {
                                    Console.WriteLine("Input invalid!");
                                    Console.ReadLine();
                                }
                            }
                            else
                            {
                                Console.WriteLine("Syarat tidak terpenuhi!");
                                Console.ReadLine();
                            }
                        }
                        else if (kata == 3)
                        {
                            if (arrMap[pilihMap].daftarKata.Count > 0)
                            {
                                for (int i = 0; i < arrMap[pilihMap].daftarKata.Count; i++)
                                {
                                    Console.WriteLine((i + 1) + ". " + arrMap[pilihMap].daftarKata[i]);
                                }
                                Console.ReadLine();
                            }
                            else
                            {
                                Console.WriteLine("Tidak terdapat kata!");
                                Console.ReadLine();
                            }
                        }
                        else if (kata == 4)
                        {
                            if (arrMap[pilihMap].daftarKata.Count > 0)
                            {
                                Console.WriteLine("Daftar Kata : ");
                                for (int i = 0; i < arrMap[pilihMap].daftarKata.Count; i++)
                                {
                                    Console.WriteLine((i + 1) + ". " + arrMap[pilihMap].daftarKata[i]);
                                }
                                Console.Write("Pilih kata yang ingin di delete : ");
                                delKata = int.Parse(Console.ReadLine());
                                delKata--;
                                arrMap[pilihMap].daftarKata.RemoveAt(delKata);
                            }
                            else
                            {
                                Console.WriteLine("Tidak terdapat kata!");
                                Console.ReadLine();
                            }
                        }
                        else if (kata == 5)
                        {
                            if (arrMap.Count > 0 && arrMap[pilihMap].listKata.Count > 0)
                            {
                                Console.WriteLine("Daftar Kata : ");
                                for (int i = 0; i < arrMap[pilihMap].listKata.Count; i++)
                                {
                                    Console.WriteLine((i + 1) + ". " + arrMap[pilihMap].listKata[i].namaKata);
                                }
                                Console.Write("Pilih kata yang ingin di delete : ");
                                indexDelete = int.Parse(Console.ReadLine());
                                indexDelete--;
                                arrMap[pilihMap].listKata[indexDelete].delete(indexDelete, arrMap[pilihMap].listKata);
                                arrMap[pilihMap].bListKata[indexDelete].delete(indexDelete, arrMap[pilihMap].bListKata);
                            }
                            else
                            {
                                Console.WriteLine("Syarat tidak terpenuhi!");
                                Console.ReadLine();
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Belum ada map!");
                        Console.ReadLine();
                    }
                }

                else if (menu == 4)
                {
                    if (arrMap.Count > 0 && arrMap[pilihMap].daftarKata.Count > 0)
                    {
                        sukses = false;
                        map bunshin = arrMap[pilihMap].clone();
                        sorting(bunshin);
                        backtrack(bunshin);
                        if (sukses == false)
                        {
                            Console.WriteLine("Tidak Ditemukan Solusi!");
                            Console.ReadLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine("Syarat tidak terpenuhi!");
                        Console.ReadLine();
                    }
                }

                else if (menu == 5)
                {
                    save();
                    Console.WriteLine("Game Saved!");
                    Console.ReadLine();
                }

                else if (menu == 6)
                {
                    load();
                    pilihMap = 0;
                    Console.WriteLine("Game Loaded!");
                    Console.ReadLine();
                }

            } while (menu != 7);

            Console.WriteLine();
            Console.WriteLine("By : Hendardi A.C      - 217116604");
            Console.WriteLine("     Jonathan Sugianto - 217116615");
            Console.WriteLine("     Matthew Gunawan   - 217116627");
            Console.WriteLine("Semoga Kami Lulus :D");
            Console.ReadLine();
        }

        static void save()
        {
            using (Stream s = File.Open(@"save.txt", FileMode.Create))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                bformatter.Serialize(s, arrMap);
            }
        }

        static void load()
        {
            using (Stream s = File.Open(@"save.txt", FileMode.Open))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                List<map> temp = (List<map>)bformatter.Deserialize(s);
                arrMap = temp;
            }
        }

        public static bool cekKosong(map m)
        {

            for (int i = 0; i < m.lebar; i++)
            {
                for (int j = 0; j < m.panjang; j++)
                {
                    if (m.kotakKosong[i, j] == true)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static void backtrack(map m)
        {
            if (m.daftarKata.Count <= 0)
            {
                sukses = true;
                Console.Clear();
                m.cetakMapBacktrack(m);
                Console.WriteLine();
                Console.WriteLine("Selesai");
                Console.ReadLine();
            }
            else
            {
                for (int i = 0; i < m.daftarKata.Count; i++)
                {
                    for (int j = 0; j < m.lebar; j++)
                    {
                        for (int k = 0; k < m.panjang; k++)
                        {
                            if (sukses == false && m.kotak[j, k] == true)
                            {
                                map bunshin = m.clone();
                                kata bK = new kata(m.daftarKata[i], k, j, 1, m.bListKata, m);

                                if (bK.validKata == true && bK.validPosisi == true)
                                {
                                    bunshin.bListKata.Add(bK);
                                    bunshin.daftarKata.RemoveAt(i);
                                    Console.Clear();
                                    bunshin.cetakMapBacktrack(bunshin);
                                    Thread.Sleep(500);
                                    backtrack(bunshin);
                                }
                                else
                                {
                                    kata bK2 = new kata(m.daftarKata[i], k, j, 2, m.bListKata, m);

                                    if (bK2.validKata == true && bK2.validPosisi == true)
                                    {
                                        bunshin.bListKata.Add(bK2);
                                        bunshin.daftarKata.RemoveAt(i);
                                        Console.Clear();
                                        bunshin.cetakMapBacktrack(bunshin);
                                        Thread.Sleep(500);
                                        backtrack(bunshin);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        static void sorting(map m)
        {
            Queue<string> queKata = new Queue<string>();

            while (m.daftarKata.Count > 0)
            {
                int cek = 0;
                int CTR = 0;
                for (int i = 0; i < m.daftarKata.Count; i++)
                {
                    if (m.daftarKata[i].Length > cek)
                    {
                        cek = m.daftarKata[i].Length;
                        CTR = i;
                    }
                }
                queKata.Enqueue(m.daftarKata[CTR]);
                m.daftarKata.RemoveAt(CTR);
            }
            while (queKata.Count > 0)
            {
                m.daftarKata.Add(queKata.Dequeue());
            }
        }
    }

    [Serializable]
    class map
    {
        public int panjang, lebar;
        public char[,] papan;
        public bool[,] kotak;
        public bool[,] kotakKosong;
        public List<kata> listKata = new List<kata>();
        public List<kata> bListKata = new List<kata>();
        public List<string> daftarKata = new List<string>();

        public map(int panjang, int lebar)
        {
            this.panjang = panjang;
            this.lebar = lebar;
            this.papan = new char[lebar, panjang];
            this.kotak = new bool[lebar, panjang];
            this.kotakKosong = new bool[lebar, panjang];

            for (int i = 0; i < lebar; i++)
            {
                for (int j = 0; j < panjang; j++)
                {
                    this.papan[i, j] = ' ';
                    this.kotak[i, j] = false;
                }
            }
        }

        public map clone()
        {
            var formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            Stream stream = new MemoryStream();

            using (stream)
            {
                formatter.Serialize(stream, this);
                stream.Seek(0, SeekOrigin.Begin);
                return (map)formatter.Deserialize(stream);
            }
        }

        public void cetakMap()
        {
            for (int i = 0; i < lebar; i++)
            {
                for (int j = 0; j < panjang; j++)
                {
                    this.papan[i, j] = ' ';
                }
            }

            foreach (kata cek in listKata)
            {
                node masuk = cek.header;

                do
                {
                    papan[masuk.y, masuk.x] = masuk.huruf;
                    if (cek.arah == 1)
                    {
                        if (masuk != null)
                        {
                            masuk = masuk.Right;
                        }
                    }
                    else
                    {
                        if (masuk != null)
                        {
                            masuk = masuk.Down;
                        }
                    }

                } while (masuk != null);
            }

            Console.Write("    ");
            for (int i = 0; i < panjang; i++)
            {
                if (i / 10 == 0)
                {
                    Console.Write(i + "  ");

                }
                else
                {
                    Console.Write(i + " ");
                }
            }
            Console.WriteLine();

            for (int i = 0; i < lebar; i++)
            {
                if (i / 10 == 0)
                {
                    Console.Write(i + " ");
                }
                else
                {
                    Console.Write(i);
                }
                Console.Write(" ");

                for (int j = 0; j < panjang; j++)
                {
                    if (kotak[i, j] == true)
                    {
                        Console.Write("[" + papan[i, j] + "]");
                    }
                    else
                    {
                        Console.Write(" " + papan[i, j] + " ");
                    }
                }
                Console.WriteLine();
            }
        }

        public void deleteMap(int index, List<map> arrMap)
        {
            arrMap.RemoveAt(index);
        }

        public void cetakMapBacktrack(map m)
        {

            for (int i = 0; i < m.lebar; i++)
            {
                for (int j = 0; j < m.panjang; j++)
                {
                    m.papan[i, j] = ' ';
                }
            }

            foreach (kata cek in bListKata)
            {
                node masuk = cek.header;

                do
                {
                    m.papan[masuk.y, masuk.x] = masuk.huruf;
                    if (cek.arah == 1)
                    {
                        if (masuk != null)
                        {
                            masuk = masuk.Right;
                        }
                    }
                    else
                    {
                        if (masuk != null)
                        {
                            masuk = masuk.Down;
                        }
                    }

                } while (masuk != null);
            }

            Console.Write("    ");
            for (int i = 0; i < m.panjang; i++)
            {
                if (i / 10 == 0)
                {
                    Console.Write(i + "  ");

                }
                else
                {
                    Console.Write(i + " ");
                }
            }
            Console.WriteLine();

            for (int i = 0; i < m.lebar; i++)
            {
                if (i / 10 == 0)
                {
                    Console.Write(i + " ");
                }
                else
                {
                    Console.Write(i);
                }
                Console.Write(" ");

                for (int j = 0; j < m.panjang; j++)
                {
                    if (m.kotak[i, j] == true)
                    {
                        Console.Write("[" + m.papan[i, j] + "]");
                    }
                    else
                    {
                        Console.Write(" " + m.papan[i, j] + " ");
                    }
                }
                Console.WriteLine();
            }
        }
    }

    [Serializable]
    class kata
    {
        public node header;
        public node curr;
        public node tempCurr;
        public int arah;
        public int panjangKata;
        public string namaKata;
        public bool ketemu;
        public bool validKata;
        public bool validPosisi;

        public kata(string inp, int x, int y, int arah, List<kata> listKata, List<map> arrMap, int pilihMap)
        {
            this.arah = arah;
            this.panjangKata = inp.Length;
            this.namaKata = inp;
            this.ketemu = false;
            this.validKata = true;
            this.validPosisi = true;

            if (arah == 1)
            {
                if (x < 0 || x + panjangKata > arrMap[pilihMap].panjang)
                {
                    validPosisi = false;
                }
            }
            else if (arah == 2)
            {
                if (y < 0 || y + panjangKata > arrMap[pilihMap].lebar)
                {
                    validPosisi = false;
                }
            }

            if (validPosisi == true)
            {
                for (int i = 0; i < panjangKata; i++)
                {
                    ketemu = false;

                    if (arah == 1)
                    {
                        if (i == 0)
                        {
                            if (x > 0 && arrMap[pilihMap].kotak[y, x - 1] == true)
                            {
                                validPosisi = false;
                            }
                        }
                        else if (i == panjangKata - 1)
                        {
                            if (x < arrMap[pilihMap].panjang - 1 && arrMap[pilihMap].kotak[y, x + 1] == true)
                            {
                                validPosisi = false;
                            }
                        }
                    }
                    else if (arah == 2)
                    {
                        if (i == 0)
                        {
                            if (y > 0 && arrMap[pilihMap].kotak[y - 1, x] == true)
                            {
                                validPosisi = false;
                            }
                        }
                        else if (i == panjangKata - 1)
                        {
                            if (y < arrMap[pilihMap].lebar - 1 && arrMap[pilihMap].kotak[y + 1, x] == true)
                            {
                                validPosisi = false;
                            }
                        }
                    }

                    if (arrMap[pilihMap].kotak[y, x] == false)
                    {
                        validPosisi = false;
                    }

                    if (validPosisi == true)
                    {
                        for (int j = 0; j < listKata.Count; j++)
                        {
                            if (arah == listKata[j].arah)
                            {
                                if (i == 0)
                                {
                                    if (arah == 1)
                                    {
                                        if (y == listKata[j].header.y && x + panjangKata > listKata[j].header.x && x < listKata[j].header.x + listKata[j].panjangKata)
                                        {
                                            validPosisi = false;
                                        }
                                    }
                                    else if (arah == 2)
                                    {
                                        if (x == listKata[j].header.x && y + panjangKata > listKata[j].header.y && y < listKata[j].header.y + listKata[j].panjangKata)
                                        {
                                            validPosisi = false;
                                        }
                                    }
                                }
                            }
                            else if (arah != listKata[j].arah)
                            {
                                for (int k = 0; k < listKata[j].panjangKata; k++)
                                {
                                    if (ketemu == false && validKata == true)
                                    {
                                        if (k == 0)
                                        {
                                            listKata[j].curr = listKata[j].header;
                                        }
                                        else
                                        {
                                            if (listKata[j].arah == 1)
                                            {
                                                listKata[j].curr = listKata[j].curr.Right;
                                            }
                                            else if (listKata[j].arah == 2)
                                            {
                                                listKata[j].curr = listKata[j].curr.Down;
                                            }
                                        }

                                        if (inp[i] != listKata[j].curr.huruf && x == listKata[j].curr.x && y == listKata[j].curr.y)
                                        {
                                            validKata = false;
                                        }

                                        if (i == 0)
                                        {
                                            if (inp[i] == listKata[j].curr.huruf && x == listKata[j].curr.x && y == listKata[j].curr.y)
                                            {
                                                header = listKata[j].curr;
                                                curr = header;
                                                ketemu = true;
                                                arrMap[pilihMap].kotakKosong[y, x] = false;
                                            }
                                        }
                                        else
                                        {
                                            if (arah == 1)
                                            {
                                                if (inp[i] == listKata[j].curr.huruf && x == listKata[j].curr.x && y == listKata[j].curr.y)
                                                {
                                                    curr.Right = listKata[j].curr;
                                                    curr = curr.Right;
                                                    ketemu = true;
                                                    arrMap[pilihMap].kotakKosong[y, x] = false;
                                                }
                                            }
                                            else if (arah == 2)
                                            {
                                                if (inp[i] == listKata[j].curr.huruf && x == listKata[j].curr.x && y == listKata[j].curr.y)
                                                {
                                                    curr.Down = listKata[j].curr;
                                                    curr = curr.Down;
                                                    ketemu = true;
                                                    arrMap[pilihMap].kotakKosong[y, x] = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (validKata == true && ketemu == false)
                        {
                            if (i == 0)
                            {
                                header = new node(inp[i], x, y, arah);
                                curr = header;
                                arrMap[pilihMap].kotakKosong[y, x] = false;
                            }
                            else
                            {
                                if (arah == 1)
                                {
                                    curr.Right = new node(inp[i], x, y, arah);
                                    curr = curr.Right;
                                    arrMap[pilihMap].kotakKosong[y, x] = false;
                                }
                                else if (arah == 2)
                                {
                                    curr.Down = new node(inp[i], x, y, arah);
                                    curr = curr.Down;
                                    arrMap[pilihMap].kotakKosong[y, x] = false;
                                }
                            }
                        }
                    }

                    if (arah == 1)
                    {
                        x++;
                    }
                    else if (arah == 2)
                    {
                        y++;
                    }
                }
            }
        }

        //Backtrack
        public kata(string inp, int x, int y, int arah, List<kata> listKata, map m)
        {
            this.arah = arah;
            this.panjangKata = inp.Length;
            this.namaKata = inp;
            this.ketemu = false;
            this.validKata = true;
            this.validPosisi = true;

            if (arah == 1)
            {
                if (x < 0 || x + panjangKata > m.panjang)
                {
                    validPosisi = false;
                }
            }
            else if (arah == 2)
            {
                if (y < 0 || y + panjangKata > m.lebar)
                {
                    validPosisi = false;
                }
            }

            if (validPosisi == true)
            {
                for (int i = 0; i < panjangKata; i++)
                {
                    ketemu = false;

                    if (arah == 1)
                    {
                        if (i == 0)
                        {
                            if (x > 0 && m.kotak[y, x - 1] == true)
                            {
                                validPosisi = false;
                            }
                        }
                        else if (i == panjangKata - 1)
                        {
                            if (x < m.panjang - 1 && m.kotak[y, x + 1] == true)
                            {
                                validPosisi = false;
                            }
                        }
                    }
                    else if (arah == 2)
                    {
                        if (i == 0)
                        {
                            if (y > 0 && m.kotak[y - 1, x] == true)
                            {
                                validPosisi = false;
                            }
                        }
                        else if (i == panjangKata - 1)
                        {
                            if (y < m.lebar - 1 && m.kotak[y + 1, x] == true)
                            {
                                validPosisi = false;
                            }
                        }
                    }

                    if (m.kotak[y, x] == false)
                    {
                        validPosisi = false;
                    }

                    if (validPosisi == true)
                    {
                        for (int j = 0; j < listKata.Count; j++)
                        {
                            if (arah == listKata[j].arah)
                            {
                                if (i == 0)
                                {
                                    if (arah == 1)
                                    {
                                        if (y == listKata[j].header.y && x + panjangKata > listKata[j].header.x && x < listKata[j].header.x + listKata[j].panjangKata)
                                        {
                                            validPosisi = false;
                                        }
                                    }
                                    else if (arah == 2)
                                    {
                                        if (x == listKata[j].header.x && y + panjangKata > listKata[j].header.y && y < listKata[j].header.y + listKata[j].panjangKata)
                                        {
                                            validPosisi = false;
                                        }
                                    }
                                }
                            }
                            else if (arah != listKata[j].arah)
                            {
                                for (int k = 0; k < listKata[j].panjangKata; k++)
                                {
                                    if (ketemu == false && validKata == true)
                                    {
                                        if (k == 0)
                                        {
                                            listKata[j].curr = listKata[j].header;
                                        }
                                        else
                                        {
                                            if (listKata[j].arah == 1)
                                            {
                                                listKata[j].curr = listKata[j].curr.Right;
                                            }
                                            else if (listKata[j].arah == 2)
                                            {
                                                listKata[j].curr = listKata[j].curr.Down;
                                            }
                                        }

                                        if (inp[i] != listKata[j].curr.huruf && x == listKata[j].curr.x && y == listKata[j].curr.y)
                                        {
                                            validKata = false;
                                        }

                                        if (i == 0)
                                        {
                                            if (inp[i] == listKata[j].curr.huruf && x == listKata[j].curr.x && y == listKata[j].curr.y)
                                            {
                                                header = listKata[j].curr;
                                                curr = header;
                                                ketemu = true;
                                                m.kotakKosong[y, x] = false;
                                            }
                                        }
                                        else
                                        {
                                            if (arah == 1)
                                            {
                                                if (inp[i] == listKata[j].curr.huruf && x == listKata[j].curr.x && y == listKata[j].curr.y)
                                                {
                                                    curr.Right = listKata[j].curr;
                                                    curr = curr.Right;
                                                    ketemu = true;
                                                    m.kotakKosong[y, x] = false;
                                                }
                                            }
                                            else if (arah == 2)
                                            {
                                                if (inp[i] == listKata[j].curr.huruf && x == listKata[j].curr.x && y == listKata[j].curr.y)
                                                {
                                                    curr.Down = listKata[j].curr;
                                                    curr = curr.Down;
                                                    ketemu = true;
                                                    m.kotakKosong[y, x] = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (validKata == true && ketemu == false)
                        {
                            if (i == 0)
                            {
                                header = new node(inp[i], x, y, arah);
                                curr = header;
                                m.kotakKosong[y, x] = false;
                            }
                            else
                            {
                                if (arah == 1)
                                {
                                    curr.Right = new node(inp[i], x, y, arah);
                                    curr = curr.Right;
                                    m.kotakKosong[y, x] = false;
                                }
                                else if (arah == 2)
                                {
                                    curr.Down = new node(inp[i], x, y, arah);
                                    curr = curr.Down;
                                    m.kotakKosong[y, x] = false;
                                }
                            }
                        }
                    }

                    if (arah == 1)
                    {
                        x++;
                    }
                    else if (arah == 2)
                    {
                        y++;
                    }
                }
            }
        }

        public void delete(int indexDelete, List<kata> listKata)
        {
            for (int i = 0; i < listKata[indexDelete].panjangKata; i++)
            {
                if (i == 0)
                {
                    listKata[indexDelete].curr = listKata[indexDelete].header;
                    if (listKata[indexDelete].arah == 1)
                    {
                        listKata[indexDelete].tempCurr = listKata[indexDelete].header.Right;
                        listKata[indexDelete].curr.Right = null;
                        listKata[indexDelete].curr = listKata[indexDelete].tempCurr;
                    }
                    else if (listKata[indexDelete].arah == 2)
                    {
                        listKata[indexDelete].tempCurr = listKata[indexDelete].header.Down;
                        listKata[indexDelete].curr.Down = null;
                        listKata[indexDelete].curr = listKata[indexDelete].tempCurr;
                    }
                }
                else if (i != 0)
                {
                    if (listKata[indexDelete].arah == 1)
                    {
                        listKata[indexDelete].tempCurr = listKata[indexDelete].curr.Right;
                        listKata[indexDelete].curr.Right = null;
                        listKata[indexDelete].curr = listKata[indexDelete].tempCurr;
                    }
                    else if (listKata[indexDelete].arah == 2)
                    {
                        listKata[indexDelete].tempCurr = listKata[indexDelete].curr.Down;
                        listKata[indexDelete].curr.Down = null;
                        listKata[indexDelete].curr = listKata[indexDelete].tempCurr;
                    }
                }
            }
            listKata[indexDelete].curr = null;
            listKata[indexDelete].tempCurr = null;
            listKata.RemoveAt(indexDelete);
        }
    }

    [Serializable]
    class node
    {
        public char huruf;
        public int x;
        public int y;
        public node Right;
        public node Down;

        public node(char huruf, int x, int y, int arah)
        {
            this.x = x;
            this.y = y;
            this.huruf = huruf;

            if (arah == 1)
            {
                Right = null;
            }
            else if (arah == 2)
            {
                Down = null;
            }
        }
    }
}
